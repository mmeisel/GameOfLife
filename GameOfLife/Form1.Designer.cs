﻿namespace GameOfLife
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.simBtn = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.trailCbx = new System.Windows.Forms.CheckBox();
            this.clrBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.patternChkx = new System.Windows.Forms.CheckBox();
            this.inspElChkBx = new System.Windows.Forms.CheckBox();
            this.stepBtn = new System.Windows.Forms.Button();
            this.speedLbl = new System.Windows.Forms.Label();
            this.steplbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.exitBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.testLbl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.linkBtn = new System.Windows.Forms.Button();
            this.rule1 = new System.Windows.Forms.Label();
            this.rule2 = new System.Windows.Forms.Label();
            this.rule3 = new System.Windows.Forms.Label();
            this.rule4 = new System.Windows.Forms.Label();
            this.instructBtn = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.Color.Black;
            this.trackBar1.Location = new System.Drawing.Point(8, 32);
            this.trackBar1.Maximum = 35;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(228, 45);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(9, 89);
            this.trackBar2.Maximum = 999;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(227, 45);
            this.trackBar2.TabIndex = 1;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // simBtn
            // 
            this.simBtn.ForeColor = System.Drawing.Color.Black;
            this.simBtn.Location = new System.Drawing.Point(9, 134);
            this.simBtn.Name = "simBtn";
            this.simBtn.Size = new System.Drawing.Size(110, 23);
            this.simBtn.TabIndex = 2;
            this.simBtn.Tag = "";
            this.simBtn.Text = "Simulate";
            this.simBtn.UseVisualStyleBackColor = true;
            this.simBtn.Click += new System.EventHandler(this.simBtn_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.ForeColor = System.Drawing.Color.Black;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Glider",
            "Small Exploder",
            "Exploder",
            "10 Cell Row",
            "Lightweight spaceship",
            "Tumbler",
            "Gosper Glider Gun",
            "Smiley face :)"});
            this.comboBox1.Location = new System.Drawing.Point(9, 208);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(227, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // trailCbx
            // 
            this.trailCbx.AutoSize = true;
            this.trailCbx.Location = new System.Drawing.Point(9, 236);
            this.trailCbx.Name = "trailCbx";
            this.trailCbx.Size = new System.Drawing.Size(103, 17);
            this.trailCbx.TabIndex = 4;
            this.trailCbx.Text = "Trail of the dead";
            this.trailCbx.UseVisualStyleBackColor = true;
            // 
            // clrBtn
            // 
            this.clrBtn.ForeColor = System.Drawing.Color.Black;
            this.clrBtn.Location = new System.Drawing.Point(9, 163);
            this.clrBtn.Name = "clrBtn";
            this.clrBtn.Size = new System.Drawing.Size(110, 23);
            this.clrBtn.TabIndex = 5;
            this.clrBtn.Tag = "";
            this.clrBtn.Text = "Clear";
            this.clrBtn.UseVisualStyleBackColor = true;
            this.clrBtn.Click += new System.EventHandler(this.clrBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.patternChkx);
            this.groupBox1.Controls.Add(this.inspElChkBx);
            this.groupBox1.Controls.Add(this.stepBtn);
            this.groupBox1.Controls.Add(this.speedLbl);
            this.groupBox1.Controls.Add(this.steplbl);
            this.groupBox1.Controls.Add(this.simBtn);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.exitBtn);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.trailCbx);
            this.groupBox1.Controls.Add(this.clrBtn);
            this.groupBox1.Controls.Add(this.trackBar1);
            this.groupBox1.Controls.Add(this.trackBar2);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(539, 261);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 276);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controls";
            // 
            // patternChkx
            // 
            this.patternChkx.AutoSize = true;
            this.patternChkx.Location = new System.Drawing.Point(121, 252);
            this.patternChkx.Name = "patternChkx";
            this.patternChkx.Size = new System.Drawing.Size(115, 17);
            this.patternChkx.TabIndex = 12;
            this.patternChkx.Text = "Pattern recognition";
            this.patternChkx.UseVisualStyleBackColor = true;
            this.patternChkx.CheckedChanged += new System.EventHandler(this.patternChkx_CheckedChanged);
            // 
            // inspElChkBx
            // 
            this.inspElChkBx.AutoSize = true;
            this.inspElChkBx.Location = new System.Drawing.Point(9, 252);
            this.inspElChkBx.Name = "inspElChkBx";
            this.inspElChkBx.Size = new System.Drawing.Size(101, 17);
            this.inspElChkBx.TabIndex = 11;
            this.inspElChkBx.Text = "Inspect element";
            this.inspElChkBx.UseVisualStyleBackColor = true;
            this.inspElChkBx.CheckedChanged += new System.EventHandler(this.inspElChkBx_CheckedChanged);
            // 
            // stepBtn
            // 
            this.stepBtn.ForeColor = System.Drawing.Color.Black;
            this.stepBtn.Location = new System.Drawing.Point(126, 134);
            this.stepBtn.Name = "stepBtn";
            this.stepBtn.Size = new System.Drawing.Size(110, 23);
            this.stepBtn.TabIndex = 9;
            this.stepBtn.Tag = "";
            this.stepBtn.Text = "Next step";
            this.stepBtn.UseVisualStyleBackColor = true;
            this.stepBtn.Click += new System.EventHandler(this.stepBtn_Click);
            // 
            // speedLbl
            // 
            this.speedLbl.AutoSize = true;
            this.speedLbl.Location = new System.Drawing.Point(152, 73);
            this.speedLbl.Name = "speedLbl";
            this.speedLbl.Size = new System.Drawing.Size(35, 13);
            this.speedLbl.TabIndex = 10;
            this.speedLbl.Text = "label6";
            // 
            // steplbl
            // 
            this.steplbl.AutoSize = true;
            this.steplbl.Location = new System.Drawing.Point(156, 237);
            this.steplbl.Name = "steplbl";
            this.steplbl.Size = new System.Drawing.Size(13, 13);
            this.steplbl.TabIndex = 9;
            this.steplbl.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(118, 237);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Step:";
            // 
            // exitBtn
            // 
            this.exitBtn.ForeColor = System.Drawing.Color.Black;
            this.exitBtn.Location = new System.Drawing.Point(125, 163);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(110, 23);
            this.exitBtn.TabIndex = 7;
            this.exitBtn.Tag = "";
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Select a predefined pattern:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Select speed of the simulation:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Select size of the matrix:";
            // 
            // testLbl
            // 
            this.testLbl.AutoSize = true;
            this.testLbl.Location = new System.Drawing.Point(536, 216);
            this.testLbl.Name = "testLbl";
            this.testLbl.Size = new System.Drawing.Size(13, 13);
            this.testLbl.TabIndex = 9;
            this.testLbl.Text = "0";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(536, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(257, 39);
            this.label4.TabIndex = 7;
            this.label4.Text = "Welcome to the game John Conway\'s Game of life. \r\nEach cell can have two possible" +
    " states: alive (yellow)\r\nor dead(gray). Rules are following:\r\n";
            // 
            // linkBtn
            // 
            this.linkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkBtn.Location = new System.Drawing.Point(539, 232);
            this.linkBtn.Name = "linkBtn";
            this.linkBtn.Size = new System.Drawing.Size(119, 23);
            this.linkBtn.TabIndex = 8;
            this.linkBtn.Text = "Links";
            this.linkBtn.UseVisualStyleBackColor = true;
            this.linkBtn.Click += new System.EventHandler(this.linkBtn_Click);
            // 
            // rule1
            // 
            this.rule1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rule1.AutoSize = true;
            this.rule1.BackColor = System.Drawing.Color.Red;
            this.rule1.ForeColor = System.Drawing.Color.White;
            this.rule1.Location = new System.Drawing.Point(536, 51);
            this.rule1.Name = "rule1";
            this.rule1.Size = new System.Drawing.Size(250, 26);
            this.rule1.TabIndex = 7;
            this.rule1.Text = " 1. Any live cell with fewer than two live neighbours \r\n     dies, as if caused b" +
    "y under-population.";
            this.rule1.DoubleClick += new System.EventHandler(this.rule1_DoubleClick);
            // 
            // rule2
            // 
            this.rule2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rule2.AutoSize = true;
            this.rule2.BackColor = System.Drawing.Color.SpringGreen;
            this.rule2.ForeColor = System.Drawing.Color.Black;
            this.rule2.Location = new System.Drawing.Point(536, 82);
            this.rule2.Name = "rule2";
            this.rule2.Size = new System.Drawing.Size(233, 26);
            this.rule2.TabIndex = 7;
            this.rule2.Text = " 2. Any live cell with two or three live neighbours\r\n     lives on to the next ge" +
    "neration.";
            this.rule2.DoubleClick += new System.EventHandler(this.rule2_DoubleClick);
            // 
            // rule3
            // 
            this.rule3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rule3.AutoSize = true;
            this.rule3.BackColor = System.Drawing.Color.Blue;
            this.rule3.ForeColor = System.Drawing.Color.White;
            this.rule3.Location = new System.Drawing.Point(536, 113);
            this.rule3.Name = "rule3";
            this.rule3.Size = new System.Drawing.Size(251, 26);
            this.rule3.TabIndex = 7;
            this.rule3.Text = " 3. Any live cell with more than three live neighbours\r\n     dies, as if by over-" +
    "population.";
            this.rule3.DoubleClick += new System.EventHandler(this.rule3_DoubleClick);
            // 
            // rule4
            // 
            this.rule4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rule4.AutoSize = true;
            this.rule4.BackColor = System.Drawing.Color.DeepPink;
            this.rule4.ForeColor = System.Drawing.Color.White;
            this.rule4.Location = new System.Drawing.Point(536, 144);
            this.rule4.Name = "rule4";
            this.rule4.Size = new System.Drawing.Size(245, 26);
            this.rule4.TabIndex = 7;
            this.rule4.Text = " 4. Any dead cell with exactly three live neighbours\r\n     becomes a live cell, a" +
    "s if by reproduction.";
            this.rule4.DoubleClick += new System.EventHandler(this.rule4_DoubleClick);
            // 
            // instructBtn
            // 
            this.instructBtn.Location = new System.Drawing.Point(660, 232);
            this.instructBtn.Name = "instructBtn";
            this.instructBtn.Size = new System.Drawing.Size(121, 23);
            this.instructBtn.TabIndex = 9;
            this.instructBtn.Text = "Instructions";
            this.instructBtn.UseVisualStyleBackColor = true;
            this.instructBtn.Click += new System.EventHandler(this.instructBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(793, 549);
            this.ControlBox = false;
            this.Controls.Add(this.instructBtn);
            this.Controls.Add(this.linkBtn);
            this.Controls.Add(this.rule4);
            this.Controls.Add(this.testLbl);
            this.Controls.Add(this.rule3);
            this.Controls.Add(this.rule2);
            this.Controls.Add(this.rule1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conway\'s Game of life";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Button simBtn;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox trailCbx;
        private System.Windows.Forms.Button clrBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button linkBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label steplbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label speedLbl;
        private System.Windows.Forms.Button stepBtn;
        private System.Windows.Forms.Label rule1;
        private System.Windows.Forms.Label rule2;
        private System.Windows.Forms.Label rule3;
        private System.Windows.Forms.Label rule4;
        private System.Windows.Forms.CheckBox inspElChkBx;
        private System.Windows.Forms.Label testLbl;
        private System.Windows.Forms.Button instructBtn;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.CheckBox patternChkx;
    }
}

