﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfLife
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            trackBar1.Value = 0;
            trackBar2.Value = 900;
            speedLbl.Text = (trackBar2.Maximum - trackBar2.Value + 1).ToString()+"ms per step";
            setupPictureboxes();
            testLbl.Visible = false;
        }
        PictureBox[,] storage;
        bool[,] previousValues;
        bool[,] currentValues;
        bool[] patternRecog = new bool[4]; //up, down, left, right
        int[,] currentRule;
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            setupPictureboxes();
            steplbl.Text = "0";
        }

        private void setupPictureboxes()
        {
            if (storage != null)
            {
                //delete all previously generated pictureboxes
                foreach (PictureBox deleteBox in storage)
                {
                    this.Controls.Remove(deleteBox);
                    deleteBox.Dispose();
                }
            }
            int x = 12, y = 12, size = (int)Math.Floor((526f - 2 * (10 + trackBar1.Value)) / (10 + trackBar1.Value));
            storage = new PictureBox[10 + trackBar1.Value, 10 + trackBar1.Value];
            previousValues = new bool[10 + trackBar1.Value, 10 + trackBar1.Value];
            currentValues = new bool[10 + trackBar1.Value, 10 + trackBar1.Value];
            currentRule=new int[10 + trackBar1.Value, 10 + trackBar1.Value];
            for (int i = 0; i < 10 + trackBar1.Value; i++)
            {
                for (int j = 0; j < 10 + trackBar1.Value; j++)
                {
                    currentRule[i, j] = 0;
                    PictureBox temp = new PictureBox();
                    temp.Location = new Point(x, y);
                    temp.Size = new Size(size, size);
                    temp.BackColor = Color.DimGray;
                    temp.SizeMode = PictureBoxSizeMode.StretchImage;
                    temp.BackgroundImageLayout = ImageLayout.Stretch;
                    temp.Click += (sendr, eTemp) =>
                    {
                       // inspElChkBx.Checked = false;
                        if (temp.BackColor == Color.DimGray) temp.BackColor = Color.Yellow;
                        else temp.BackColor = Color.DimGray;
                    };
                    temp.MouseEnter += (sendr, eTemp) =>
                    {
                        if(timer1.Enabled==false && inspElChkBx.Checked==true)
                        {
                            int locX, locY;
                            locX = ((temp.Location.X + temp.Size.Height) / (temp.Size.Height+2)) - 1;
                            locY = ((temp.Location.Y + temp.Size.Height) / (temp.Size.Height+2)) - 1;
                            testLbl.Text = locX.ToString() + "-" + locY.ToString();
                            for(int a=-1;a<=1;a++)
                            {
                                for(int b=-1;b<=1;b++)
                                {
                                    if (locX+a>=0 && locY+b>=0 && locX+a<=9+trackBar1.Value && locY+b<=9+trackBar1.Value)
                                    {
                                        #region InspectElementBorder
                                        if (a==-1)
                                        {
                                            if(b==-1) storage[locX+a,locY+b].Image=Properties.Resources.leftTop;
                                            else if (b==0) storage[locX+a,locY+b].Image=Properties.Resources.left;
                                            else storage[locX + a, locY + b].Image = Properties.Resources.leftBot;                                            
                                        }
                                        else if(a==0)
                                        {
                                            if (b == -1) storage[locX+a,locY+b].Image=Properties.Resources.top;
                                            else if (b == 1) storage[locX + a, locY + b].Image = Properties.Resources.bot;
                                        }
                                        else
                                        {
                                            if (b == -1) storage[locX + a, locY + b].Image = Properties.Resources.rightTop;
                                            else if (b == 0) storage[locX + a, locY + b].Image = Properties.Resources.right;
                                            else storage[locX + a, locY + b].Image = Properties.Resources.rightBot;
                                        }
                                        #endregion
                                        if (currentRule[locX + a, locY + b] == 1) storage[locX+a,locY+b].BackColor=rule1.BackColor;
                                        else if (currentRule[locX + a, locY + b] == 2) storage[locX + a, locY + b].BackColor = rule2.BackColor;
                                        else if (currentRule[locX + a, locY + b] == 3) storage[locX + a, locY + b].BackColor = rule3.BackColor;
                                        else if (currentRule[locX + a, locY + b] == 4) storage[locX + a, locY + b].BackColor = rule4.BackColor;
                                    }
                                }
                            }
                        }
                    };
                    temp.MouseLeave += (sendr, eTemp) =>
                      {
                          if (timer1.Enabled == false && inspElChkBx.Checked == true)
                          {
                              int locX, locY;
                              locX = ((temp.Location.X + temp.Size.Height) / (temp.Size.Height + 2)) - 1;
                              locY = ((temp.Location.Y + temp.Size.Height) / (temp.Size.Height + 2)) - 1;
                              testLbl.Text = locX.ToString() + "-" + locY.ToString();
                              for (int a = -1; a <= 1; a++)
                              {
                                  for (int b = -1; b <= 1; b++)
                                  {
                                      if (locX + a >= 0 && locY + b >= 0 && locX + a <= 9 + trackBar1.Value && locY + b <= 9 + trackBar1.Value)
                                      {
                                          storage[locX + a, locY + b].Image = null;
                                          if (currentValues[locX + a, locY + b] == true) storage[locX + a, locY + b].BackColor = Color.Yellow;
                                          else storage[locX + a, locY + b].BackColor = Color.DimGray;
                                      }
                                  }
                              }
                          }
                      };
                    temp.BackColorChanged += (sendr, eTemp) =>
                      {
                          if(timer1.Enabled==false)
                          {
                              int locX, locY;
                              locX = ((temp.Location.X + temp.Size.Height) / (temp.Size.Height + 2)) - 1;
                              locY = ((temp.Location.Y + temp.Size.Height) / (temp.Size.Height + 2)) - 1;
                              if (temp.BackColor == Color.Yellow) currentValues[locX, locY] = true;
                              else if (temp.BackColor == Color.DimGray) currentValues[locX, locY] = false;
                          }
                      };
                    storage[i, j] = temp;
                    this.Controls.Add(temp);

                    y += size + 2;
                }
                x += size + 2;
                y = 12;
            }
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            speedLbl.Text = (trackBar2.Maximum - trackBar2.Value + 1).ToString()+"ms per step";
            timer1.Interval = trackBar2.Maximum - trackBar2.Value+1;
        }

        private void simBtn_Click(object sender, EventArgs e)
        {
            if (simBtn.Text == "Simulate")
            {
                simBtn.Text = "Stop simulation";
                stepBtn.Enabled = false;
             //   init();
                //steplbl.Text = "0";
                timer1.Enabled = true;
            }
            else
            {
                simBtn.Text = "Simulate";
                timer1.Enabled = false;
                stepBtn.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            runStep();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            simBtn.Text = "Simulate";
            timer1.Enabled = false;
            stepBtn.Enabled = true;
            steplbl.Text = "0";
            Array.Clear(currentRule, 0, currentRule.Length);
            if (comboBox1.SelectedIndex == 0) //glider
            {
                if (trackBar1.Value != 15)
                {
                    trackBar1.Value = 15;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                storage[2, 1].BackColor = Color.Yellow;
                storage[3, 2].BackColor = Color.Yellow;
                storage[1, 3].BackColor = Color.Yellow;
                storage[2, 3].BackColor = Color.Yellow;
                storage[3, 3].BackColor = Color.Yellow;
            }
            else if (comboBox1.SelectedIndex == 1) //small exploder
            {
                if (trackBar1.Value != 5)
                {
                    trackBar1.Value = 5;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                storage[7, 6].BackColor = Color.Yellow;
                storage[7, 7].BackColor = Color.Yellow;
                storage[6, 7].BackColor = Color.Yellow;
                storage[8, 7].BackColor = Color.Yellow;
                storage[6, 8].BackColor = Color.Yellow;
                storage[8, 8].BackColor = Color.Yellow;
                storage[7, 9].BackColor = Color.Yellow;

            }
            else if (comboBox1.SelectedIndex == 2) //exploder
            {
                   if (trackBar1.Value != 5)
                    {
                        trackBar1.Value = 5;
                        setupPictureboxes();
                    }
                    else
                    {
                        foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                    }
                    storage[7, 5].BackColor = Color.Yellow;
                    storage[7, 9].BackColor = Color.Yellow;
                    for (int j = 0; j < 5; j++)
                    {
                        storage[5, 5 + j].BackColor = Color.Yellow;
                        storage[9, 5 + j].BackColor = Color.Yellow;
                    }

            }
            else if (comboBox1.SelectedIndex == 3) //10 cell row
            {
                if (trackBar1.Value != 6)
                {
                    trackBar1.Value = 6;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                for (int i = 0; i < 10; i++) storage[3+i, 7].BackColor = Color.Yellow;

            }
            else if (comboBox1.SelectedIndex == 4) //Lightweight spaceship
            {
                if (trackBar1.Value !=20)
                {
                    trackBar1.Value = 20;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                storage[0, 10].BackColor = Color.Yellow;
                storage[0, 12].BackColor = Color.Yellow;
                storage[1, 9].BackColor = Color.Yellow;
                storage[2, 9].BackColor = Color.Yellow;
                storage[3, 9].BackColor = Color.Yellow;
                storage[4, 9].BackColor = Color.Yellow;
                storage[4, 10].BackColor = Color.Yellow;
                storage[4, 11].BackColor = Color.Yellow;
                storage[3, 12].BackColor = Color.Yellow;

            }
            else if (comboBox1.SelectedIndex == 5) //Tumbler
            {
                if (trackBar1.Value != 5)
                {
                    trackBar1.Value = 5;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                for(int j=0;j<5;j++)
                {
                    storage[6, 5 + j].BackColor = Color.Yellow;
                    storage[8, 5 + j].BackColor = Color.Yellow;
                }
                storage[5,5].BackColor = Color.Yellow;
                storage[5,6].BackColor = Color.Yellow;
                storage[9,5].BackColor = Color.Yellow;
                storage[9,6].BackColor = Color.Yellow;
                storage[5,10].BackColor = Color.Yellow;
                storage[4,10].BackColor = Color.Yellow;
                storage[4,9].BackColor = Color.Yellow;
                storage[4, 8].BackColor = Color.Yellow;
                storage[9, 10].BackColor = Color.Yellow;
                storage[10, 10].BackColor = Color.Yellow;
                storage[10, 9].BackColor = Color.Yellow;
                storage[10, 8].BackColor = Color.Yellow;

            }

            else if (comboBox1.SelectedIndex == 6) //Gosper glider gun
            {
                if (trackBar1.Value != 35)
                {
                    trackBar1.Value = 35;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                int x = 2, y = 12;
                storage[x,y].BackColor = Color.Yellow;
                storage[x,y+1].BackColor = Color.Yellow;
                storage[x+1,y].BackColor = Color.Yellow;
                storage[x+1,y+1].BackColor = Color.Yellow;

                storage[x+9,y].BackColor = Color.Yellow;
                storage[x+10,y].BackColor = Color.Yellow;
                storage[x+10,y+1].BackColor = Color.Yellow;
                storage[x+8,y+1].BackColor = Color.Yellow;
                storage[x + 8, y + 2].BackColor = Color.Yellow;
                storage[x + 9, y + 2].BackColor = Color.Yellow;

                storage[x+16,y+2].BackColor = Color.Yellow;
                storage[x+16,y+3].BackColor = Color.Yellow;
                storage[x+16,y+4].BackColor = Color.Yellow;
                storage[x+17,y+2].BackColor = Color.Yellow;
                storage[x+18,y+3].BackColor = Color.Yellow;

                storage[x+22,y].BackColor = Color.Yellow;
                storage[x+23,y].BackColor = Color.Yellow;
                storage[x+22,y-1].BackColor = Color.Yellow;
                storage[x+23,y-2].BackColor = Color.Yellow;
                storage[x+24,y-2].BackColor = Color.Yellow;
                storage[x+24,y-1].BackColor = Color.Yellow;

                storage[x+24,y+10].BackColor = Color.Yellow;
                storage[x+25,y+10].BackColor = Color.Yellow;
                storage[x+26,y+10].BackColor = Color.Yellow;
                storage[x+24,y+11].BackColor = Color.Yellow;
                storage[x+25,y+12].BackColor = Color.Yellow;

                storage[x+34, y-1].BackColor = Color.Yellow;
                storage[x+35, y - 1].BackColor = Color.Yellow;
                storage[x+34, y-2].BackColor = Color.Yellow;
                storage[x +35, y -2].BackColor = Color.Yellow;

                storage[x+35,y+5].BackColor = Color.Yellow;
                storage[x+35,y+6].BackColor = Color.Yellow;
                storage[x+35,y+7].BackColor = Color.Yellow;
                storage[x+36,y+5].BackColor = Color.Yellow;
                storage[x+37,y+6].BackColor = Color.Yellow;
            }
            else if (comboBox1.SelectedIndex == 7)
            {
                if (trackBar1.Value != 5)
                {
                    trackBar1.Value = 5;
                    setupPictureboxes();
                }
                else
                {
                    foreach (PictureBox temp in storage) temp.BackColor = Color.DimGray;
                }
                storage[6,3].BackColor = Color.Yellow;
                storage[8,3].BackColor = Color.Yellow;
                storage[7,5].BackColor = Color.Yellow;
                storage[7,7].BackColor = Color.Yellow;
                storage[6,7].BackColor = Color.Yellow;
                storage[8,7].BackColor = Color.Yellow;
                storage[5,6].BackColor = Color.Yellow;
                storage[9,6].BackColor = Color.Yellow;
            }
        }

        private void clrBtn_Click(object sender, EventArgs e)
        {
            simBtn.Text = "Simulate";
            stepBtn.Enabled = true;
            timer1.Enabled = false;
            steplbl.Text = "0";
            Array.Clear(currentRule, 0, currentRule.Length);
            foreach (PictureBox clearBox in storage)
            {
                clearBox.BackColor = Color.DimGray;
                clearBox.BackgroundImage = null;
            }
            for (int x = 0; x < 10 + trackBar1.Value; x++)
                for (int y = 0; y < 10 + trackBar1.Value; y++) currentRule[x, y] = 0;
        }

        private void linkBtn_Click(object sender, EventArgs e)
        {
            About aboutFom = new About();
            aboutFom.ShowDialog();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void stepBtn_Click(object sender, EventArgs e)
        {
           // init();
            runStep();
        }

        #region Init and run

        private void runStep ()
        {
            int brojac;
            int boundary = (int)Math.Sqrt(storage.Length) - 1;
            Array.Copy(currentValues, previousValues, currentValues.Length);
            Array.Clear(currentRule, 0, currentRule.Length);
            for (int i = 0; i < Math.Sqrt(storage.Length); i++)
            {
                for (int j = 0; j < Math.Sqrt(storage.Length); j++)
                {
                    brojac = 0;
                    if (i > 0)
                    {
                        if (previousValues[i - 1, j] == true) brojac++;
                        if (j > 0) if (previousValues[i - 1, j - 1] == true) brojac++;
                        if (j < boundary) if (previousValues[i - 1, j + 1] == true) brojac++;
                    }
                    if (i < boundary)
                    {
                        if (previousValues[i + 1, j] == true) brojac++;
                        if (j > 0) if (previousValues[i + 1, j - 1] == true) brojac++;
                        if (j < boundary) if (previousValues[i + 1, j + 1] == true) brojac++;
                        if (j > 0) if (previousValues[i, j - 1] == true) brojac++;
                        if (j < boundary) if (previousValues[i, j + 1] == true) brojac++;

                    }

                    if ((brojac < 2 || brojac > 3) && previousValues[i, j] == true)
                    {
                        currentValues[i, j] = false;
                        if (brojac < 2) currentRule[i, j] = 1;
                        else if(brojac>3) currentRule[i, j] = 3;
                    }
                    else if (previousValues[i, j] == false && brojac == 3)
                    {
                        currentValues[i, j] = true;
                        currentRule[i, j] = 4;
                    }
                    else if((brojac==2 || brojac==3) && previousValues[i,j]==true) currentRule[i, j] = 2;
                    if (currentValues[i, j] == true) storage[i, j].BackColor = Color.Yellow;
                    else if (previousValues[i, j] == true && currentValues[i, j] == false && trailCbx.Checked == true) storage[i, j].BackColor = Color.DarkGreen;
                    else if (storage[i, j].BackColor != Color.DarkGreen) storage[i, j].BackColor = Color.DimGray;
                }
            }
            #region Pattern Recognition
            if (patternChkx.Checked==true)
            {
                for (int i = 0; i < Math.Sqrt(storage.Length); i++)
                {
                    for (int j = 0; j < Math.Sqrt(storage.Length); j++)
                    {
                        storage[i, j].BackgroundImage = null;
                        if (currentValues[i, j] == true)
                        {
                            if (i > 0)
                            {
                                if (currentValues[i - 1, j] == true) patternRecog[2] = false;
                                else patternRecog[2] = true;
                            }
                            else patternRecog[2] = true;
                            if (i < boundary)
                            {
                                if (currentValues[i + 1, j] == true) patternRecog[3] = false;
                                else patternRecog[3] = true;
                            }
                            else patternRecog[3] = true;
                            if (j > 0)
                            {
                                if (currentValues[i, j - 1] == true) patternRecog[0] = false;
                                else patternRecog[0] = true;
                            }
                            else patternRecog[0] = true;
                            if (j < boundary)
                            {
                                if (currentValues[i, j + 1] == true) patternRecog[1] = false;
                                else patternRecog[1] = true;
                            }
                            else patternRecog[1] = true;

                            if (patternRecog[0] == true && patternRecog[1] == true && patternRecog[2] == true && patternRecog[3] == true) storage[i, j].BackgroundImage = Properties.Resources.c;
                            else if (patternRecog[0])
                            {
                                if (patternRecog[2])
                                {
                                    if (patternRecog[3]) storage[i, j].BackgroundImage = Properties.Resources.blank_b;
                                    else if (patternRecog[1]) storage[i, j].BackgroundImage = Properties.Resources.blank_r;
                                    else storage[i, j].BackgroundImage = Properties.Resources.tl;
                                }
                                else if (patternRecog[3])
                                {
                                    if (patternRecog[1]) storage[i, j].BackgroundImage = Properties.Resources.blank_l;
                                    else storage[i, j].BackgroundImage = Properties.Resources.tr;
                                }
                                else if (patternRecog[1]) storage[i, j].BackgroundImage = Properties.Resources.tb;
                                else storage[i, j].BackgroundImage = Properties.Resources.t;

                            }
                            else if (patternRecog[1])
                            {
                                if (patternRecog[2])
                                {
                                    if (patternRecog[3]) storage[i, j].BackgroundImage = Properties.Resources.blank_t;
                                    else storage[i, j].BackgroundImage = Properties.Resources.bl;
                                }
                                else if (patternRecog[3]) storage[i, j].BackgroundImage = Properties.Resources.br;
                                else storage[i, j].BackgroundImage = Properties.Resources.b;
                            }
                            else if (patternRecog[2])
                            {
                                if (patternRecog[3]) storage[i, j].BackgroundImage = Properties.Resources.lr;
                                else storage[i, j].BackgroundImage = Properties.Resources.l;
                            }
                            else if (patternRecog[3]) storage[i, j].BackgroundImage = Properties.Resources.r;
                        }

                    }
                }
                #endregion
            }

            steplbl.Text = (int.Parse(steplbl.Text) + 1).ToString();
        }

        private void init()
        {
            for (int i = 0; i < Math.Sqrt(storage.Length); i++)
            {
                for (int j = 0; j < Math.Sqrt(storage.Length); j++)
                {
                    if (storage[i, j].BackColor == Color.Yellow || currentRule[i,j]==2 || currentRule[i,j]==4) currentValues[i, j] = true;
                    else currentValues[i, j] = false;
                }
            }
        }
        #endregion

        private void inspElChkBx_CheckedChanged(object sender, EventArgs e)
        {
            //if(inspElChkBx.Checked==true) MessageBox.Show("You decided to use inspect element.\nWith this tool you can inspect rules applied in the current step for a cell and his neighbours. Colors are displayed in the rules while gray cells weren't involved in this step.\nInspect element can only be used while simulation is not running. Clicking on any cell will disable this tool.","Inspect element",MessageBoxButtons.OK, MessageBoxIcon.Information);
            if(inspElChkBx.Checked==false)
                foreach (PictureBox pbx in storage) pbx.Image = null;
        }

        private void instructBtn_Click(object sender, EventArgs e)
        {
            inst instructions = new inst();
            instructions.Show();
        }

        #region Rule_DoubleClick - Color Dialog
        private void rule1_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) rule1.BackColor = colorDialog1.Color;
        }

        private void rule2_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) rule2.BackColor = colorDialog1.Color;
        }

        private void rule3_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) rule3.BackColor = colorDialog1.Color;
        }

        private void rule4_DoubleClick(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) rule4.BackColor = colorDialog1.Color;
        }
        #endregion

        private void patternChkx_CheckedChanged(object sender, EventArgs e)
        {
            if (patternChkx.Checked==false)
            {
                foreach (PictureBox str in storage) str.BackgroundImage = null;
            }
        }
    }
}
